package cz.fi.muni.pa165.currency;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Currency;
import java.util.Locale;
import javax.naming.TimeLimitExceededException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mockito;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyConvertorImplTest {
    
    private static Currency CZK = Currency.getInstance("CZK");
    private static Currency EUR = Currency.getInstance("EUR");
    
    @Mock
    private ExchangeRateTable exchange;
    
    private CurrencyConvertor convertor;
    
    @Before
    public void init() {
        convertor = new CurrencyConvertorImpl(exchange);
}

    @Test
    public void testConvert() throws ExternalServiceFailureException {
        // Don't forget to test border values and proper rounding.
         
         //Currency sourceCurrency = Mockito.mock(Currency.class);
         Currency sourceCurrency = Currency.getInstance(Locale.ITALY);
         Currency targetCurrency = Currency.getInstance(Locale.CANADA);
         //Currency targetCurrency = Mockito.mock(Currency.class);
         BigDecimal sourceAmount = Mockito.mock(BigDecimal.class);
         
         //exchange = Mockito.mock(ExchangeRateTable.class);
         
         //when(exchange.getExchangeRate(sourceCurrency, targetCurrency)).thenThrow(new ExternalServiceFailureException("sss"));
         //when(exchange.getExchangeRate(CZK, EUR)).thenReturn(new BigDecimal(BigInteger.ONE));
         when(exchange.getExchangeRate(EUR, CZK)).thenReturn(new BigDecimal("0.1"));
         
       // CurrencyConvertorImpl convertor = new CurrencyConvertorImpl(exchange);
        //assertEquals(new BigDecimal(BigInteger.ONE), convertor.convert(sourceCurrency, targetCurrency, sourceAmount));
        assertEquals(new BigDecimal("1.00"), convertor.convert(EUR, CZK, new BigDecimal("10.05")));
        assertEquals(new BigDecimal("1.01"), convertor.convert(EUR, CZK, new BigDecimal("10.06")));
        
        //ASSERTJ
        
        
    }

    @Test
    public void testConvertWithNullSourceCurrency() {
        fail("Test is not implemented yet.");
    }

    @Test
    public void testConvertWithNullTargetCurrency() {
        fail("Test is not implemented yet.");
    }

    @Test
    public void testConvertWithNullSourceAmount() {
        fail("Test is not implemented yet.");
    }

    @Test
    public void testConvertWithUnknownCurrency() {
        fail("Test is not implemented yet.");
    }

    @Test
    public void testConvertWithExternalServiceFailure() {
        fail("Test is not implemented yet.");
    }

}
